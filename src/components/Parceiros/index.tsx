import Image from 'next/image'
import styles from './styles.module.css'

export default function Parceiros() {
  return (
    <section id="parceiros" className={styles.section}>
      <div className={styles.inner}>
        <h2 className={styles.title}>Parceiros</h2>
        <div className={styles.logos}>
          <div className={styles.logo}>
            <Image src="/images/logos/stella.png" alt="Stella Artois" width={532} height={91} />
          </div>
          <div className={styles.logo}>
            <Image src="/images/logos/99.png" alt="99" width={180} height={180} />
          </div>
          <div className={styles.logo}>
            <Image src="/images/logos/looks.png" alt="Looks Kreative Studio" width={446} height={278} />
          </div>
        </div>
      </div>
    </section>
  )
}
