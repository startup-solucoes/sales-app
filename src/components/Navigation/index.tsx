import Image from 'next/image'
import styles from './styles.module.css'

export default function Navigation() {
  return (
    <nav id="menu" className={styles.section}>
      <div className={styles.inner}>
        <div className={styles.logo}>
          <Image
            alt="Curitiba na Caixa"
            src="/images/logos/curitiba-na-caixa.svg"
            width={176}
            height={153}
            priority
          />
        </div>
        <ul>
          <li><a href="#como-funciona">Como funciona</a></li>
          <li><a href="#edicoes">Últimas edições</a></li>
          <li><a href="#proposito">Propósito</a></li>
          <li><a href="#parceiros">Parceiros</a></li>
          <li><a href="#surpresa">Assine</a></li>
        </ul>
      </div>
    </nav>
  )
}
