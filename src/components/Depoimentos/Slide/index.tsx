/* eslint-disable react/no-danger */
/* eslint-disable max-len */
import Image from 'next/image'
import styles from './styles.module.css'

export default function DepoimentoSlide(props) {
  const { image, name, content } = props
  return (
    <div className={styles.item}>
      <div className={styles.box}>
        <div className={styles.image}>
          <Image src={image} quality={90} alt={name} width={669} height={646} layout="intrinsic" />
        </div>
        <div className={styles.body}>
          <h3 className={styles.title}>{name}</h3>
          <div className={styles.content} dangerouslySetInnerHTML={{ __html: content }} />
        </div>
      </div>
    </div>
  )
}
