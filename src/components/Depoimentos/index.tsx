/* eslint-disable max-len */
/* eslint-disable react/jsx-props-no-spreading */
import Slider from 'react-slick'
import DepoimentoSlide from '@/components/Depoimentos/Slide'

import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'
import styles from './styles.module.css'

const depoimentosList = [
  {
    name: 'Kauana Bechtloff',
    content: '<p>Curitiba na Caixa é surpreendente. Impossível não sorrir quando ela chega porque você sabe que vem recheada de coisas maravilhosas! A variedade e a qualidade dos produtos são incríveis. É inevitável compartilhar com os amigos para mostrar: “prova esse, é maravilhoooso”.</p><p>Obrigada Curitiba na Caixa por valorizar nossos produtores locais e trazer de modo fácil até os consumidores a oportunidade de conhecer tantas coisas boas!</p>',
    image: '/images/testimonials/kauana.png',
  },
  {
    name: 'Arthur Fraga',
    content: '<p>Receber produtos com uma qualidade ímpar que são tipicamente curitibanos, selecionados com tanto carinho faz qualquer dia se tornar único. O mix de expectativa somado a surpresa da chegada só torna CURITIBA NA CAIXA uma experiência sem igual.</p>',
    image: '/images/testimonials/arthur.png',
  },
  {
    name: 'Juliana Reinhardt',
    content: '<p>Curitiba na Caixa é uma das melhores ideias para quem quiser ter uma explosão de surpresa e alegria. Todo mês você inicia um acompanhamento das delícias que você vai receber. Então acontece toda uma preparação para o tão esperado dia de chegada da Caixa. E quando chega... é uma experiência inesquecível!!!</p>',
    image: '/images/testimonials/juliana.png',
  },
  {
    name: 'Yannih Tsushima',
    content: '<p>Adorei a novidade! Amei conhecer novos lugares e delícias diferentes que não conhecia aqui na nossa cidade e o melhor, sem sair de casa. ❤️ Parabéns Curitiba na Caixa por apoiar o comércio local.</p>',
    image: '/images/testimonials/yannih.png',
  },
  {
    name: 'Lucas Garcia',
    content: '<p>Mais uma vez o O Que Fazer Curitiba inovando e criando experiências únicas para nós curitibanos! O Curitiba Na Caixa é uma ótima oportunidade de conhecermos tudo de melhor que nossa cidade oferece! Bem diversificado com doces e salgados, e tudo absolutamente delicioso. A vontade de repetir era enorme!</p>',
    image: '/images/testimonials/lucas.jpeg',
  },
  {
    name: 'Débora Silva',
    content: '<p>Como sempre o OQFC inovando e se adaptando a todas as fases! O Curitiba na caixa não poderia ser diferente, foi uma das experiências mais deliciosas que eu já tive, pude experimentar um pouquinho de cada delícia, feita com tanto carinho e amor por empreendedores de Curitiba, resumindo, uma caixa cheia de amor e doçura! Com certeza serei uma eterna assinante.</p>',
    image: '/images/testimonials/debora.png',
  },
  {
    name: 'Gra Stahl',
    content: '<p>Fala sério Brasil! Quem não ficaria feliz recebendo em casa uma caixa dessas? Comprei o pacote trimestral e a primeira caixa foi incrível. No segundo e terceiro mês, desmemoriada que sou, já havia esquecido que a tal caixa viria, e um dia do nada, pá, A CAIXA! Foi bom demaaaaais! Produtos de qualidade e fresquíssimos.</p><p>Ótimo custo-benefício. Indico muito!</p>',
    image: '/images/testimonials/gra.jpeg',
  },
  {
    name: 'André Henning Ferreira',
    content: '<p>Curitiba na Caixa foi uma das iniciativas mais incríveis que poderíamos ter participado, a ideia de apoiar os empreendimentos locais e conhecer ainda mais da nossa cidade é de fato muito importante e delicioso! Parabéns para equipe OQFC por ter nos envolvido nesse mega projeto!</p>',
    image: '/images/testimonials/andre.jpeg',
  },
  {
    name: 'Larissa Marques',
    content: '<p>Receber Curitiba na caixa, além de agradar meu dia, me reconectou com momentos de café da tarde, algo que por motivo de reunião e trabalho não fazia há algum tempo! A caixa chegou por volta das 17h recheada de marcas deliciosas que me fizeram correr pro fogão passar um café.  Outro detalhe que me fez amar essa experiência encaixotinha em forma de amor, foi a cor deslumbrante da caixa! Não tinha como dar errado ao abrir! Obrigada gente por encantar em cada detalhe.</p>',
    image: '/images/testimonials/larissa.png',
  },
  {
    name: 'Rita Severino',
    content: '<p>Curitiba na CAIXA!!!!<br />Gostei tanto da ideia que na estreia fui de tri! Sucesso! Amamos!<br />E na sequência outras mais ...meus amigos precisavam conhecer uma caixa pensada e embalada com tanto carinho, seu nome escrito à mão!!!!! Delicadeza!!! Um passeio por Curitiba empreendedora, sabores, cores, novos lugares! Escolhas em harmonia! Presente certeiro! E tudo entregue na data, perfeito e fresquinho!<br />Visita esperada com aquele sabor de surpresa!!! Hoje tem Curitiba na Caixa?</p>',
    image: '/images/testimonials/rita.jpeg',
  },
]

export default function Depoimentos() {
  const settings = {
    arrows: false,
    dots: true,
    fade: true,
    infinite: true,
    speed: 1000,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 5000,
  };
  return (
    <section id="depoimentos" className={styles.section}>
      <div className={styles.body}>
        <h2 className={styles.title}>O que dizem sobre nós</h2>
        <div className={styles.slides}>
          <Slider {...settings}>
            {depoimentosList.map((slide) => <DepoimentoSlide key={slide.name} {...slide} />)}
          </Slider>
        </div>
      </div>
    </section>
  )
}
