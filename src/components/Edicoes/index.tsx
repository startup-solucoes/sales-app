import Image from 'next/image'
import CheckoutButton from '@/components/CheckoutButton'
import styles from './styles.module.css'

export default function Edicoes({ isClosedSales }) {
  return (
    <section id="edicoes" className={styles.section}>
      <div className={styles.inner}>
        <h2 className={styles.title}>Últimas edições</h2>
        <div className={styles.editions}>
          <div className={styles.editions_event}>
            <Image src="/images/editions/janeiro.png" alt="Edição de Janeiro" width={380} height={380} />
            <h3 className={styles.edition_title}>Edição de Janeiro</h3>
          </div>
          <div className={styles.editions_event}>
            <Image src="/images/editions/dezembro.png" alt="Edição de Dezembro" width={380} height={380} />
            <h3 className={styles.edition_title}>Edição de Dezembro</h3>
          </div>
          <div className={styles.editions_event}>
            <Image src="/images/editions/novembro.png" alt="Edição de Novembro" width={380} height={380} />
            <h3 className={styles.edition_title}>Edição de Novembro</h3>
          </div>
        </div>
        <div className={styles.cta}>
          <CheckoutButton isClosedSales={isClosedSales} checkoutUnique={false} ctaLabel="Assine Agora" />
        </div>
      </div>
    </section>
  )
}
