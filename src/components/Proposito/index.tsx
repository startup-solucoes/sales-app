import styles from './styles.module.css'

export default function Proposito() {
  return (
    <section id="proposito" className={styles.section}>
      <div className={styles.inner}>
        <div className={styles.content}>
          <h2 className={styles.title}>Propósito</h2>
          <div className={styles.description}>
            <p>
              A gente bem sabe que empreender não é nada fácil. Para ajudar quem continua dando
              tudo de si para continuar com as portas abertas, selecionamos as mais gostosas e
              criativas iniciativas como forma de apoiarmos, juntos, os negócios locais que
              fazem a cidade acontecer.
            </p>
            <p>
              Um convite a pensar &quot;fora da caixa&quot;, apoiando o pequeno e abrindo
              espaço para o novo.
            </p>
          </div>
        </div>
      </div>
    </section>
  )
}
