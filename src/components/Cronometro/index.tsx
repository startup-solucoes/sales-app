import { useEffect, useState } from 'react'
import { Countdown } from '@/helpers/countdown'
import styles from './styles.module.css'

export default function Cronometro({ cronometro, isClosedSales }) {
  const [timer, setTimer] = useState(cronometro)

  useEffect(() => {
    if (isClosedSales) return;
    setInterval(() => {
      setTimer(Countdown(cronometro.dateEndString))
    }, 1000)
  }, [])

  return (
    <nav id="cronometro" className={styles.section}>
      <div className={styles.inner}>
        {!isClosedSales && <div className={styles.label}>Esgota em</div>}
        {!isClosedSales && <div className={styles.timer}>{timer.inText}</div>}
        {isClosedSales && <div className={styles.label}>Esgotado!</div>}
        {isClosedSales && <div className={styles.timer}>Abriremos novamente em breve</div>}
      </div>
    </nav>
  )
}
