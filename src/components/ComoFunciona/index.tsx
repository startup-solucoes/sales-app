/* eslint-disable react/jsx-props-no-spreading */
import Card from '@/components/ComoFunciona/Card'
import styles from './styles.module.css'

const cardsCollection = [
  {
    title: 'Como Funciona',
    content: 'São 6 produtos incrivelmente deliciosos que você já amava – ou nem fazia ideia que existiam – que ficam ainda mais gostosos quando compartilhados. Seja no tapete da sala, na mesa da cozinha ou em meio às cobertas do quarto, podemos apostar que vai ser especial.',
    image: 'como-funciona',
    isHorizontal: true,
  },
  {
    title: 'Participantes Selecionados',
    content: 'Os melhores estabelecimentos de Curitiba escolhidos a dedo para compor a caixinha mais gostosa da cidade.',
    image: 'participantes-selecionados',
  },
  {
    title: 'Economia',
    content: 'Além daquele delicioso friozinho na barriga de nem imaginar o que virá na caixa, a praticidade de receber tudo sem sair de casa e por um preço muito mais especial que comprar tudo separado.',
    image: 'economia',
  },
  {
    title: 'Apoie Empreendedores Locais',
    content: 'A gente bem sabe que empreender não é nada fácil. Por isso, adquirir a caixinha é, também, dar um voto de apoio para que todas essas iniciativas incríveis de gente que faz a cidade, continuem a acontecer.',
    image: 'apoie-empreendedores-locais',
  },
  {
    title: 'Momentos Especiais',
    content: 'Nossa caixinha foi criada para matar as saudades de lugares que tanto amamos e, ainda, outros que vamos amar descobrir juntos. E melhor do que guardar esse momento pra si, é ter com quem compartilhar, não é?!',
    image: 'momentos-especiais',
  },
];

export default function ComoFunciona() {
  return (
    <section id="como-funciona" className={styles.section}>
      <div className={styles.inner}>

        {cardsCollection.map((cardProps) => <Card key={cardProps.title} {...cardProps} />)}

      </div>
    </section>
  )
}
