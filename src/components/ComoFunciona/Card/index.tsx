import Image from 'next/image';
import styles from './styles.module.css'

type CardProps = {
  title: string;
  content: string;
  image: string;
  isHorizontal?: boolean;
}

function Card(props: CardProps) {
  const {
    title, content, image, isHorizontal,
  } = props;
  return (
    <div className={styles.card} data-is-horizontal={isHorizontal}>
      <div className={styles.image}>
        <Image src={`/images/icons/${image}.png`} alt={title} layout="fill" objectFit="contain" objectPosition="center bottom" />
      </div>
      <div className={styles.body}>
        <h2 className={styles.title}>{title}</h2>
        <p className={styles.content}>{content}</p>
      </div>
    </div>
  )
}

Card.defaultProps = {
  isHorizontal: false,
}

export default Card
