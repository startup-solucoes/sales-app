/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
import { createContext } from 'react';

export interface IModalContext {
  visible: boolean;
  header?: string;
  body?: JSX.Element | string;
}

export const modalDefaultValue: IModalContext = {
  visible: false,
  header: 'Aguarde!',
  body: <p>Carregando...</p>,
}

const ModalContext = createContext({
  modal: modalDefaultValue,
  setModal: (_modal: IModalContext) => {},
})
export default ModalContext
