/* eslint-disable no-console */
/* eslint-disable max-len */
import { useContext } from 'react'
import ModalContext from '@/components/Modal/context'
import styles from './styles.module.css'

export default function Modal() {
  const { modal } = useContext(ModalContext)
  if (!modal.visible) return null
  return (
    <div className={styles.backdrop}>
      <div className={styles.backdrop__container}>
        <div className="fixed inset-0 transition-opacity" aria-hidden="true">
          <div className="absolute inset-0 bg-gray-700 opacity-75" />
        </div>
        <span className="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">&#8203;</span>

        <div className={styles.modal} role="dialog" aria-modal="true" aria-labelledby="modal-headline">
          <div className={styles.header}>
            <h3 className={styles.title} id="modal-headline">
              {modal.header}
            </h3>
          </div>
          <div className={styles.body}>
            {modal.body}
          </div>
        </div>
      </div>
    </div>
  )
}
