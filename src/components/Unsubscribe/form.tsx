/* eslint-disable no-console */
import axios from 'axios';
import { useContext, useState } from 'react';
import ModalContext from '@/components/Modal/context'
import styles from './styles.module.css'

export default function UnsubscribeModal() {
  const { setModal } = useContext(ModalContext)
  const [email, setEmail] = useState('')

  function closeModal(e) {
    e.preventDefault()
    setModal({ visible: false })
  }

  // eslint-disable-next-line no-unused-vars
  const stepError = (
    <div className={styles.form}>
      <section>
        <p>Não foi possível concluir está solicitação por causa de um problema técnico.</p>
        <p>Tente novamente mais tarde ou entre em contato conosco solicitando o cancelamento.</p>
      </section>
      <footer>
        <button type="button" onClick={closeModal}>
          Fechar
        </button>
      </footer>
    </div>
  )

  const stepWaiting = (
    <div className={styles.form}>
      <section>
        <p>Aguarde, processando...</p>
      </section>
    </div>
  )

  const stepSuccess = (
    <div className={styles.form}>
      <section>
        <p>Ok, encontramos sua assinatura.</p>
        <p>Enviamos um e-mail para você com um link onde você pode cancelar a assinatura.</p>
        <p>Esperamos que você retorne, em breve!</p>
      </section>
      <footer>
        <button type="button" onClick={closeModal}>
          Fechar
        </button>
      </footer>
    </div>
  )

  async function onSubmitForm(e) {
    e.preventDefault()

    setModal({
      visible: true,
      header: 'Cancelar Assinatura',
      body: stepWaiting,
    })

    try {
      const response = await axios.post('/api/cancel', { email });
      console.log(response)
      setModal({
        visible: true,
        header: 'Cancelar Assinatura',
        body: stepSuccess,
      })
    } catch (err) {
      console.log(err)
      setModal({
        visible: true,
        header: 'Cancelar Assinatura',
        body: stepError,
      })
    }
  }

  return (
    <div className={styles.form}>
      <section>
        <label htmlFor="form-unsubscribe-email" className={styles.control}>
          <div className={styles.label}>Informe o e-mail cadastrado na assinatura</div>
          <input
            type="text"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            className={styles.input}
            id="form-unsubscribe-email"
            placeholder="jon.snow@westeros.com"
          />
        </label>
      </section>
      <footer>
        <button type="button" onClick={onSubmitForm} className={styles.confirm}>
          Confirmar
        </button>
        <button type="button" onClick={closeModal} className={styles.cancel}>
          Cancelar
        </button>
      </footer>
    </div>
  )
}
