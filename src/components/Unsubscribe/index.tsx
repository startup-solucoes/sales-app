import { useContext } from 'react';
import ModalContext from '@/components/Modal/context'
import UnsubscribeForm from '@/components/Unsubscribe/form'
import styles from './styles.module.css'

export default function Unsubscribe() {
  const { setModal } = useContext(ModalContext)

  function openUnsubscribeForm(e) {
    e.preventDefault()
    setModal({
      visible: true,
      header: 'Cancelar Assinatura',
      body: <UnsubscribeForm />,
    })
  }

  return (
    <section id="unsubscribe" className={styles.section}>
      <div className={styles.inner}>
        <h2 className={styles.title}>Quer cancelar sua assinatura?</h2>
        <div className={styles.body}>
          <button
            type="button"
            className={styles.button_open}
            onClick={openUnsubscribeForm}
          >
            Solicitar Cancelamento
          </button>
        </div>
      </div>
    </section>
  )
}
