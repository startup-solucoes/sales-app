import Navigation from '@/components/Navigation'
import CheckoutButton from '@/components/CheckoutButton'

import styles from './styles.module.css'

export default function Hero({ isClosedSales }) {
  return (
    <section id="hero" className={styles.section}>
      <div className={styles.bg}>
        <div className={styles.inner}>
          <Navigation />
          <div className={styles.content}>
            <div className={styles.body}>
              <h2 className={styles.title}>Seu novo jeito de aproveitar Curitiba:</h2>
              <h3 className={styles.subtitle}>
                Uma caixa recheada de sabores e experiências únicas, sem sair de casa
              </h3>
              <div className={styles.cta_buttons}>
                <CheckoutButton isClosedSales={isClosedSales} checkoutUnique ctaLabel="Quero testar" />
                <CheckoutButton isClosedSales={isClosedSales} checkoutUnique={false} ctaLabel="Quero assinar" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}
