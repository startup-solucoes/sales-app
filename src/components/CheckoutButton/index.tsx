/* eslint-disable no-console */
import { useContext } from 'react';
import ModalContext from '@/components/Modal/context'
import { simpleCheckoutProcess, simpleSendToPagarme } from '@/services/payment/checkout/simple'
import { recurrentCheckoutProcess, recurrentSendToPagarme } from '@/services/payment/checkout/recurrent'
import ClosedSalesModal from '@/components/CheckoutButton/closed-sales'

import styles from './styles.module.css'

export default function CheckoutButton({ checkoutUnique, ctaLabel, isClosedSales }) {
  const { setModal } = useContext(ModalContext)

  function handleError(data) {
    console.error(data)
  }

  async function handleSuccess(data) {
    const result = (checkoutUnique)
      ? await simpleSendToPagarme(data)
      : await recurrentSendToPagarme(data)

    setModal({
      visible: true,
      header: result.header,
      body: result.body,
    })
  }

  function openClosedSalesModal() {
    setModal({
      visible: true,
      header: 'Vendas Encerradas',
      body: <ClosedSalesModal />,
    })
  }

  function openCheckout(e) {
    e.preventDefault()
    if (isClosedSales) {
      openClosedSalesModal()
      return
    }
    if (checkoutUnique) {
      simpleCheckoutProcess(handleSuccess, handleError)
    } else {
      recurrentCheckoutProcess(handleSuccess, handleError)
    }
  }

  return (
    <button
      type="button"
      className={checkoutUnique ? styles.button : styles.button_blue}
      onClick={(e) => openCheckout(e)}
    >
      {ctaLabel}
    </button>
  )
}
