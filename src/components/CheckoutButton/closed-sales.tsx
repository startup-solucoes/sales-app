/* eslint-disable no-console */
import { useContext } from 'react';
import ModalContext from '@/components/Modal/context'
import styles from './styles.module.css'

export default function ClosedSalesModal() {
  const { setModal } = useContext(ModalContext)

  function closeModal(e) {
    e.preventDefault()
    setModal({ visible: false })
  }

  return (
    <div className={styles.modal}>
      <section>
        {/* eslint-disable-next-line react/jsx-one-expression-per-line */}
        <p>As vendas para <strong>este mês</strong> estão encerradas.</p>
        <p>Você poderá comprar novamente em breve.</p>
      </section>
      <footer>
        <button type="button" onClick={closeModal}>
          Fechar
        </button>
      </footer>
    </div>
  )
}
