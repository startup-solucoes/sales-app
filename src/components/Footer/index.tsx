import styles from './styles.module.css'

export default function Footer() {
  return (
    <section className={styles.section}>
      <div className={styles.inner}>
        O QUE FAZER CURITIBA 2021® – Todos os Direitos Reservados. Criado por Agência Disco
      </div>
    </section>
  )
}
