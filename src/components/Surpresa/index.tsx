import CheckoutButton from '@/components/CheckoutButton'
import styles from './styles.module.css'

export default function Surpresa({ isClosedSales }) {
  return (
    <section id="surpresa" className={styles.section}>
      <div className={styles.inner}>
        <div className={styles.content}>
          <h2 className={styles.title}>
            Todos os meses uma deliciosa surpresa em sua casa
          </h2>
          <div className={styles.cta_buttons}>
            <CheckoutButton isClosedSales={isClosedSales} checkoutUnique ctaLabel="Quero testar" />
            <CheckoutButton isClosedSales={isClosedSales} checkoutUnique={false} ctaLabel="Quero assinar" />
          </div>
        </div>
      </div>
    </section>
  )
}
