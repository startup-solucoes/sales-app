/* eslint-disable react/jsx-props-no-spreading */
import CardPlano from '@/components/Planos/Card'
import styles from './styles.module.css'

export default function Planos({ sendDateText, isClosedSales }) {
  const cardsCollection = [
    {
      isClosedSales,
      title: 'Plano único',
      price: 99,
      priceLegend: '+ frete',
      checkoutUnique: true,
      ctaLabel: 'Quero Testar',
      listItems: [
        'Frete R$ 15,00',
        'Envio único',
        'Participantes escolhidos a dedo',
        'Produtos exclusivos para você',
        'Receba a experiência em casa',
        'Economize',
        'Apoie empreendedores locais',
        'Envio agendado para o seu endereço',
        `Próxima entrega no dia ${sendDateText}`,
      ],
    },
    {
      isClosedSales,
      title: 'Assinatura mensal',
      price: 89,
      priceLegend: 'mensal + frete',
      checkoutUnique: false,
      ctaLabel: 'Quero Assinar',
      listItems: [
        'Frete R$ 15,00 por mês',
        'Uma caixa por mês',
        'Participantes escolhidos a dedo',
        'Todo mês produtos exclusivos para você',
        'Receba a experiência em casa',
        'Economize',
        'Apoie empreendedores locais',
        'Envio agendado para o seu endereço',
        `Próxima entrega no dia ${sendDateText}`,
        'Brindes exclusivos',
        'Comunidade exclusiva',
        'Cancelamento sem burocracia e quando quiser',
      ],
    },
  ];

  return (
    <section id="planos" className={styles.section}>
      <div className={styles.inner}>

        <h2 className={styles.title}>Planos</h2>

        <div className={styles.items}>
          {cardsCollection.map((cardProps) => <CardPlano key={cardProps.title} {...cardProps} />)}
        </div>

      </div>
    </section>
  )
}
