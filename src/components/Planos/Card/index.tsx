/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable no-console */
/* eslint-disable max-len */
import CheckoutButton from '@/components/CheckoutButton'
import styles from './styles.module.css'

type CardProps = {
  title: string
  price: number
  priceLegend: string
  listItems: string[]
  ctaLabel: string
  checkoutUnique: boolean,
  isClosedSales: boolean,
}

export default function CardPlano(props: CardProps) {
  const {
    title, price, priceLegend, listItems, isClosedSales,
  } = props;

  return (
    <div className={styles.card}>
      <div className={styles.body}>
        <h2 className={styles.title}>{title}</h2>
        <div className={styles.price}>
          <div className={styles.price_value}>
            <small>R$</small>
            {price}
          </div>
          <div className={styles.price_legend}>{priceLegend}</div>
        </div>
        <ul className={styles.list}>
          {listItems.map((item) => <li key={item} className={styles.listitem}>{item}</li>)}
        </ul>
        <div className={styles.cta}>
          <CheckoutButton isClosedSales={isClosedSales} {...props} />
        </div>
      </div>
    </div>
  )
}
