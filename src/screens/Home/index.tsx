/* eslint-disable max-len */
/* eslint-disable no-unused-vars */
import { useState } from 'react'
import Head from 'next/head'

import Modal from '@/components/Modal'
import ModalContext, { modalDefaultValue } from '@/components/Modal/context'
import Cronometro from '@/components/Cronometro'
import Hero from '@/components/Hero'
import ComoFunction from '@/components/ComoFunciona'
import Edicoes from '@/components/Edicoes'
import Planos from '@/components/Planos'
import Proposito from '@/components/Proposito'
import Parceiros from '@/components/Parceiros'
import Surpresa from '@/components/Surpresa'
import Depoimentos from '@/components/Depoimentos'
import Unsubscribe from '@/components/Unsubscribe'
import Footer from '@/components/Footer'

export default function Home({ cronometro, sendDateText, isClosedSales }) {
  const [modal, setModal] = useState(modalDefaultValue)

  return (
    <ModalContext.Provider value={{
      modal,
      setModal,
    }}
    >
      <main>
        <Cronometro cronometro={cronometro} isClosedSales={isClosedSales} />
        <Hero isClosedSales={isClosedSales} />
        <ComoFunction />
        <Edicoes isClosedSales={isClosedSales} />
        <Depoimentos />
        <Planos isClosedSales={isClosedSales} sendDateText={sendDateText} />
        <Proposito />
        <Parceiros />
        <Surpresa isClosedSales={isClosedSales} />
      </main>
      <Unsubscribe />
      <Footer />
      <Modal />
      <Head>
        <title>Curitiba Na Caixa - Seu novo jeito de aproveitar Curitiba</title>
        <script defer src="https://assets.pagar.me/checkout/1.1.0/checkout.js" />
      </Head>
    </ModalContext.Provider>
  )
}
