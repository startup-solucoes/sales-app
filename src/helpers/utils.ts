/* eslint-disable import/prefer-default-export */
export function getRequestUrl(req) {
  const { host } = req.headers
  const protocol = (host.indexOf('localhost') > -1)
    ? 'http'
    : 'https'
  const url = `${protocol}://${host}`
  return {
    host,
    protocol,
    url,
  }
}
