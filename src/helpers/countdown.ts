export function Countdown(dateEndString: string) {
  const dateEnd = new Date(dateEndString)
  // const targetDate = new Date(dateEnd).getTime() + (1000 * 60 * 60 * 24 * 365)
  const targetDate = new Date(dateEnd).getTime()
  const now = new Date().getTime()

  let delta = Math.abs(targetDate - now) / 1000

  const days = Math.floor(delta / 86400)
  delta -= days * 86400

  const hours = Math.floor(delta / 3600) % 24
  delta -= hours * 3600

  const minutes = Math.floor(delta / 60) % 60
  delta -= minutes * 60

  const seconds = Math.floor(delta % 60)

  const inText = `${days} Dias : ${hours} Horas : ${minutes} Minutos : ${seconds} Segundos`
  return {
    seconds,
    minutes,
    hours,
    days,
    inText,
    dateEndString,
  }
}

export function isFutureDateTime(dateString): boolean {
  const targetDate = new Date(dateString).getTime()
  const now = new Date().getTime()
  return (targetDate > now)
}
