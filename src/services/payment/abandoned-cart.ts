/* eslint-disable no-console */
import { differenceInDays, set } from 'date-fns'

import { sendBoletoReminder, sendBoletoReminderLastDay } from '@/services/mail'

export default async function sendRecoverEmail(transaction) {
  // transaction.boleto_expiration_date = '2021-03-31T03:00:00.000Z'
  const dueDate = new Date(transaction.boleto_expiration_date)
  const newDate = set(new Date(), {
    hours: 0, minutes: 0, seconds: 0, milliseconds: 0,
  })
  const timeLeft = differenceInDays(dueDate, newDate)

  // if (timeLeft <= 24 && timeLeft > 0) {
  // } else if (timeLeft <= 0 && timeLeft > -24) {
  if (timeLeft === 1) {
    await sendBoletoReminder(transaction)
  } else if (timeLeft === 0) {
    await sendBoletoReminderLastDay(transaction)
  } else {
    const error = {
      message: 'Nenhuma regra aplicável para esta transação ou o Boleto já venceu',
      data: {
        boleto_expiration_date: transaction.boleto_expiration_date,
        timeLeft,
      },
    }
    throw error
  }

  return {
    message: 'Email de lembrete enviado com sucesso',
    data: {
      name: transaction.customer.name,
      email: transaction.customer.email,
      boleto_expiration_date: transaction.boleto_expiration_date,
      timeLeft,
    },
  }
}
