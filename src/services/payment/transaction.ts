/* eslint-disable no-console */
import axios from 'axios'

axios.defaults.headers.post.Accept = 'application/json'
axios.defaults.headers.post['Content-Type'] = 'application/json'

export default async function getTransaction(transactionId) {
  try {
    const BASE_URL = `https://api.pagar.me/1/transactions/${transactionId}?api_key=${process.env.PAGARME_API_TOKEN}`

    /**
     * Faz a solicitação de captura
     */
    const result = await axios.get(BASE_URL)
    // console.log(result.data)

    /**
     * Retorno
     */
    return result.data
  } catch (error) {
    /**
     * Deu ruim!
     */
    console.log(error.response.data);
    console.log(error)
    throw error
  }
}
