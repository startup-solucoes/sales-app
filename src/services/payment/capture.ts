/* eslint-disable no-console */
import axios from 'axios'

axios.defaults.headers.post.Accept = 'application/json'
axios.defaults.headers.post['Content-Type'] = 'application/json'

export default async function capture(body) {
  console.log(body)
  try {
    /**
     * Pega o token do Checkout
     * Monta o endpoint de captura
     */
    const TRANSACTION_TOKEN = body.token
    const AMOUNT = 11400
    const BASE_URL = `https://api.pagar.me/1/transactions/${TRANSACTION_TOKEN}/capture?api_key=${process.env.PAGARME_API_TOKEN}&amount=${AMOUNT}`
    // console.log(BASE_URL)

    /**
     * Faz a solicitação de captura
     */
    const result = await axios.post(BASE_URL)
    // console.log(result.data)

    /**
     * Retorno
     */
    return result.data
  } catch (error) {
    /**
     * Deu ruim!
     */
    console.log(error.response.data);
    console.log(error)
    throw error
  }
}
