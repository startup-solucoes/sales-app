/* eslint-disable no-console */
import { createHmac } from 'crypto';
import { NextApiRequest } from 'next';

export function postbackValidate(req: NextApiRequest) {
  // return true
  const rawBody = (new URLSearchParams(req.body)).toString()
  const signature = createHmac('sha1', process.env.PAGARME_API_TOKEN).update(rawBody).digest('hex')
  return (`sha1=${signature}` === req.headers['x-hub-signature']);
}

export default function postback(req: NextApiRequest) {
  try {
    const postbackData = req.body
    // console.log(postbackData);

    /**
     * Valida a assinatura do postback
     */
    // if (!postbackValidate(req)) {
    //   console.log('== POSTBACK VALIDATE ==');
    //   console.log(req.headers);

    //   const error = {
    //     statusCode: 401,
    //     body: {
    //       statusCode: 401,
    //       message: 'Assinatura do Postback inválida',
    //     },
    //   }
    //   throw error
    // }

    /**
     * Faz a solicitação de captura
     */
    if (postbackData.current_status !== 'paid') {
      const error = {
        statusCode: 200,
        body: {
          statusCode: 200,
          message: 'Nenhuma ação necessária para este status',
          postback: postbackData,
        },
      }
      throw error
    }

    /**
     * Retorno
     */
    return {
      statusCode: 200,
      body: {
        statusCode: 200,
        message: 'Processado com sucesso',
      },
    }
  } catch (err) {
    console.log(err)
    if (err.statusCode && err.body) throw err

    const error = {
      statusCode: 500,
      body: {
        statusCode: 500,
        message: 'Ops! Houston, temos um problema!',
        errors: err,
      },
    }
    throw error
  }
}
