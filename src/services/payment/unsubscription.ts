/* eslint-disable no-underscore-dangle */
/* eslint-disable no-console */
import crypto from 'crypto'
import axios from 'axios'

axios.defaults.headers.post.Accept = 'application/json'
axios.defaults.headers.post['Content-Type'] = 'application/json'

export async function searchSubscription(email: string) {
  try {
    const queryRaw = {
      query: {
        filtered: {
          query: {
            match_phrase: {
              'customer.email': {
                query: email,
              },
            },
          },
          filter: {
            and: [
              {
                term: {
                  status: 'paid',
                },
              },
            ],
          },
        },
      },
      sort: [
        {
          date_created: {
            order: 'desc',
          },
        },
      ],
      size: 1,
      from: 0,
    }
    const query = JSON.stringify(queryRaw)
    const BASE_URL = `https://api.pagar.me/1/search?api_key=${process.env.PAGARME_API_TOKEN}&type=subscription&query=${query}`
    const result = await axios.get(BASE_URL)
    return (result.data.hits.total)
      ? result.data.hits.hits[0]._source
      : false
  } catch (error) {
    console.log(error.response.data);
    throw error.response.data
  }
}

export async function unsubscription(subscriptionId: number) {
  try {
    const BASE_URL = `https://api.pagar.me/1/subscriptions/${subscriptionId}/cancel`
    const result = await axios.post(BASE_URL, {
      api_key: process.env.PAGARME_API_TOKEN,
    })
    console.log(result.data)

    return result.data
  } catch (error) {
    console.log(error.response.data);
    throw error.response.data
  }
}

export function createHash(data, encoding: crypto.BinaryToTextEncoding = 'hex') {
  const hmac = crypto.createHmac('sha256', process.env.PAGARME_API_CRYPT);
  hmac.update(data);
  return hmac.digest(encoding);
}
