/* eslint-disable camelcase */
/* eslint-disable no-console */
import axios from 'axios';
import pagarmeCheckout from '@/services/payment/checkout/pagarme'

export function recurrentCheckoutProcess(handleSuccess, handleError) {
  const checkout = pagarmeCheckout(handleSuccess, handleError)
  checkout.open({
    createToken: 'false',
    amount: 10400,
    customerData: 'true',
    paymentMethods: 'credit_card',
    uiColor: '#5668E6',
    headerText: 'Assinatura Mensal',
    postbackUrl: `${window.location.origin}/api/postback`,
  });
}

type ModalResult = {
  header: string,
  body: string,
}

export async function recurrentSendToPagarme(data) {
  let result: ModalResult

  try {
    const res = await axios({
      method: 'post',
      url: `${window.location.origin}/api/subscription`,
      data,
      responseType: 'json',
    });

    console.log(res.data)

    result = {
      header: 'Assinatura realizada!',
      body: `Sua assinatura está sendo processada!
        \nVocê receberá uma confirmação por email assim que o processamento for concluído.
        \nNão esqueça de verificar também sua caixa de Spam.`,
    }
  } catch (error) {
    console.error(error.response.data);
    result = {
      header: 'Ops! Tivemos um problema.',
      body: 'Houve um problema na requisição e não conseguimos processar sua assinatura.',
    }
  }

  return result
}
