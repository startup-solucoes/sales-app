/* eslint-disable camelcase */
/* eslint-disable no-console */
import axios from 'axios'
import pagarmeCheckout from '@/services/payment/checkout/pagarme'

export function simpleCheckoutProcess(handleSuccess, handleError) {
  const checkout = pagarmeCheckout(handleSuccess, handleError)
  checkout.open({
    createToken: 'true',
    amount: 11400,
    customerData: 'true',
    capture: 'true',
    paymentMethods: 'credit_card boleto pix',
    maxInstallments: 1,
    freeInstallments: 1,
    defaultInstallments: 1,
    uiColor: '#2FA62F',
    headerText: 'Plano Único',
    postbackUrl: `${window.location.origin}/api/postback`,
    items: [{
      id: '1',
      title: 'Plano único',
      unit_price: 9900,
      quantity: 1,
      tangible: 'true',
    }],
  });
}

type ModalResult = {
  header: string,
  body: string,
}

export async function simpleSendToPagarme(data) {
  const { token, payment_method } = data;

  let result: ModalResult

  try {
    const res = await axios({
      method: 'post',
      url: `${window.location.origin}/api/capture`,
      data: { token, payment_method },
      responseType: 'json',
    });

    console.log(res.data)

    /**
     * Se for Boleto
     */
    if (payment_method === 'boleto') {
      if (res.data.boleto_url) {
        result = {
          header: 'Compra realizada!',
          body: `Você receberá o Boleto por e-mail.
            \nVocê será avisado por e-mail assim que recebermos a confirmação do pagamento,
            o que pode levar até 4 dias úteis.`,
        }
      }
    } else {
      /**
       * Se for Cartão
       */
      result = {
        header: 'Compra realizada!',
        body: `Sua compra está sendo processada!
          \nVocê receberá uma confirmação por email assim que o processamento for concluído.
          \nNão esqueça de verificar também sua caixa de Spam.`,
      }
    }
  } catch (error) {
    console.error(error.response.data);
    result = {
      header: 'Ops! Tivemos um problema.',
      body: 'Houve um problema na requisição e não conseguimos processar sua compra.',
    }
  }

  return result
}
