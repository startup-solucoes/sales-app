export default function pagarmeCheckout(handleSuccess, handleError) {
  // eslint-disable-next-line dot-notation
  const PagarMeCheckout = window['PagarMeCheckout'] || { Checkout: {} }
  const encryptionKey = (window.location.hostname === 'curitibanacaixa.com.br')
    ? 'ek_live_UTIpRQ4EZOwKSG5qqdcHAnKlSrPEVv'
    : 'ek_test_87ftfVqobYtvDlTcsAEZF4wx0UTpNR'
  const checkout = new PagarMeCheckout.Checkout({
    encryption_key: encryptionKey,
    success: handleSuccess,
    error: handleError,
  });
  return checkout
}
