/* eslint-disable no-console */
import axios from 'axios'

axios.defaults.headers.post.Accept = 'application/json'
axios.defaults.headers.post['Content-Type'] = 'application/json'

export default async function subscription(checkout: object) {
  try {
    const BASE_URL = 'https://api.pagar.me/1/subscriptions'

    // eslint-disable-next-line dot-notation, no-param-reassign
    checkout['api_key'] = process.env.PAGARME_API_TOKEN

    const result = await axios.post(BASE_URL, checkout)
    console.log(result.data)

    return result.data
  } catch (error) {
    console.log(error.response.data);
    console.log(error)
    throw error
  }
}
