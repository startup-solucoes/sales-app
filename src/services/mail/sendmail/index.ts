/* eslint-disable no-unused-vars */
/* eslint-disable import/no-extraneous-dependencies */
import ses from '@/services/mail/sendmail/ses'
import { MailContent } from '@/services/types/mail'

export default async (email: string, content: MailContent) => {
  try {
    const response = await ses(email, content)
    return response
  } catch (error) {
    return error
  }
}
