/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable no-console */
import { SES, Credentials } from 'aws-sdk'
import { ApiResponse } from '@/services/types/response'
import { MailContent } from '@/services/types/mail'
import errorMessages from './errorMessages.json';

const region = process.env.AWS_REGION || 'us-east-1'
const credentials = new Credentials(
  process.env.MY_AWS_ACCESS_KEY_ID,
  process.env.MY_AWS_SECRET_ACCESS_KEY,
)
const client = new SES({ region, credentials })

export default async (email: string, content: MailContent) => {
  let response: ApiResponse
  try {
    const emailParams: SES.SendEmailRequest = {
      Destination: {
        ToAddresses: [email],
      },
      Message: {
        Body: {
          Html: {
            Charset: 'UTF-8',
            Data: content.body,
          },
        },
        Subject: {
          Data: content.subject,
        },
      },
      Source: process.env.SES_SOURCE_MAIL,
    };
    console.log(emailParams);
    await client.sendEmail(emailParams).promise()

    response = {
      success: true,
      statusCode: 200,
      message: 'AWS.SES: Email envia com sucesso',
    };
  } catch (error) {
    response = {
      success: false,
      statusCode: error.statusCode,
      message: `AWS.SES: ${errorMessages[error.code]}`,
    };
  }
  return response
}
