import { MailContent } from '@/services/types/mail'
import template from '@/services/mail/template'

type MailProps = {
  name: string,
  number: number,
  link: string,
  code: string,
  items: any[],
  amount: number
}

export default (props: MailProps) : MailContent => {
  const content = template('Pague seu Boleto', `
<p>Oieee! ${props.name},</p>
<p>Passando aqui pra só pra te lembrar do boleto mais doce e gostoso que você tem com a gente!</p>
<p>Todas as delícias estão perto de chegar, por isso não se esqueça dele viu?! rsrs</p>
<p>Depois de efetuado o pagamento é só relaxar e esperar que logo logo batemos aí na sua porta 🤪</p>
<p>Segue o link do boleto:<br />${props.link}</p>
<p>No Internet Banking, utilize o código de barras:<br /><code>${props.code}</code></p>
<p>Caso já tenha efetuado o pagamento ao ler este e-mail, por favor, desconsidere essa mensagem.</p>
<p>Qualquer dúvida é só nos chamar,<br />Obrigado</p>
`, '#5668e6', '#FFFFFF')

  return {
    subject: 'Pague seu Boleto - Curitiba na Caixa',
    body: content,
  }
}
