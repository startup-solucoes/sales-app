import { MailContent } from '@/services/types/mail'
import template from '@/services/mail/template'
import formatItems from '@/services/mail/content/formatItems'

type MailProps = {
  name: string,
  number: number,
  plan: any,
}

export default (props: MailProps) : MailContent => {
  const items = [
    {
      quantity: 1,
      title: props.plan.name,
      unit_price: props.plan.amount,
    },
  ]
  const contentItems = formatItems(items, null, false)
  const content = template('Assinatura Aprovada', `
<p>HEY ${props.name},</p>
<p>Temos uma ótima notícia: sua assinatura (${props.number}) foi aprovada!</p>
${contentItems}
<p>Já estamos super animados em ver vocês por aqui, agora é só esperar um pouquinho que vamos preparar sua caixa com os melhores produtos da cidade!</p>
<p>Ah, as entregas são feitas sempre na última semana do mês, tá?</p>
<p>Enquanto isso, siga @curitibanacaixa no Instagram e fique por dentro de todas as novidades :)</p>
`, '#0087E6', '#FFFFFF')

  return {
    subject: 'Assinatura Aprovada - Curitiba na Caixa',
    body: content,
  }
}
