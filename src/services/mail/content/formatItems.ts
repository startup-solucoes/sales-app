function formatNumber(amount: number) : string {
  return amount.toString().replace(/^([\d]+)([\d]{2})$/, '$1,$2')
}

export default (items: any[], amount?: number, withFrete: boolean = true) : string => {
  const frete = 1500
  let result = '<table width="100%" class="order-details">'
  result += '<thead><tr><td>Descrição</td><td class="align-right">Qtd</td><td class="align-right">Valor</td></tr></thead>'
  result += '<tbody>'
  items.forEach((item) => {
    result += `<tr><td>${item.title}</td><td class="align-right">${item.quantity}</td><td class="align-right">R$ ${formatNumber(item.unit_price)}</td></tr>`;
  })
  if (withFrete) {
    result += `<tr><td>Frete</td><td class="align-right"></td><td class="align-right">R$ ${formatNumber(frete)}</td></tr>`
  }
  if (amount) {
    // const total = (withFrete) ? (amount + frete) : amount
    result += `<tr><td>Total</td><td class="align-right"></td><td class="align-right">R$ ${formatNumber(amount)}</td></tr>`
  }
  result += '</tbody></table>'
  return result
}
