import { MailContent } from '@/services/types/mail'
import template from '@/services/mail/template'

type MailProps = {
  name: string,
  link: string,
}

export default (props: MailProps) : MailContent => {
  const content = template('Cancelar Assinatura', `
<p>Olá ${props.name},</p>
<p>Recebemos o seu pedido cancelamento da assinatura.</p>
<p>Esperamos que você tenha gostado de estar com a gente e que possa retornar em breve.<br />Foi um prazer ter você conosco!</p>
<p>Para confirmar o cancelamento acesso o link e vá em <i>"Cancelar assinatura"</i>: ${props.link}</p>
`, '#af0b48', '#FFFFFF')

  return {
    subject: 'Cancelar Assinatura - Curitiba na Caixa',
    body: content,
  }
}
