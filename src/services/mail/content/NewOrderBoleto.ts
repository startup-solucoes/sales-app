import { MailContent } from '@/services/types/mail'
import template from '@/services/mail/template'
import formatItems from '@/services/mail/content/formatItems'

type MailProps = {
  name: string,
  number: number,
  link: string,
  code: string,
  items: any[],
  amount: number
}

export default (props: MailProps) : MailContent => {
  const contentItems = formatItems(props.items, props.amount)
  const content = template('Aguardando Pagamento', `
<p>Oie ${props.name},</p>
<p>Recebemos o seu pedido (${props.number}).</p>
${contentItems}
<p>Aproveite para pagar seu boleto e garantir a caixa mais desejada da cidade!</p>
<p>Segue o link do boleto: ${props.link}</p>
<p>ou</p>
<p>No Internet Banking, utilize o código de barras: ${props.code}</p>
`, '#E69f00', '#FFFFFF')

  return {
    subject: 'Aguardando Pagamento - Curitiba na Caixa',
    body: content,
  }
}
