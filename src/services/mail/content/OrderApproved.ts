import { MailContent } from '@/services/types/mail'
import template from '@/services/mail/template'
import formatItems from '@/services/mail/content/formatItems'

type MailProps = {
  name: string,
  number: number,
  items: any[],
  amount: number,
}

export default (props: MailProps) : MailContent => {
  const contentItems = formatItems(props.items, props.amount)
  const content = template('Pedido Aprovado', `
<p>HEY ${props.name},</p>
<p>Temos uma ótima notícia: seu pedido (${props.number}) foi aprovado!</p>
${contentItems}
<p>Já estamos super animados em ver vocês por aqui, agora é só esperar um pouquinho que vamos preparar sua caixa com os melhores produtos da cidade!</p>
<p>Ah, as entregas são feitas sempre na última semana do mês, tá?</p>
<p>Enquanto isso, siga @curitibanacaixa no Instagram e fique por dentro de todas as novidades :)</p>
`, '#328200', '#FFFFFF')

  return {
    subject: 'Pedido Aprovado - Curitiba na Caixa',
    body: content,
  }
}
