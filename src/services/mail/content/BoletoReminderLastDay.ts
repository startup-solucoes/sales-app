import { MailContent } from '@/services/types/mail'
import template from '@/services/mail/template'

type MailProps = {
  name: string,
  number: number,
  link: string,
  code: string,
  items: any[],
  amount: number
}

export default (props: MailProps) : MailContent => {
  const content = template('Hoje vence seu Boleto', `
<p>Heey! ${props.name},</p>
<p>Como está? Tudo certo por ai?</p>
<p>Passando só pra te recordar que é hoje que vence seu boleto! Não vai esquecer heim (:</p>
<p>Estamos aqui bolando uma edição incrível para você se deliciar em casa e descobrir Curitiba de uma forma única !</p>
<p>Para facilitar o seu pagamento, disponibilizamos ao final desse e-mail um código de barras!</p>
<p>Não esquece, abre agora o app do banco e finalize sua compra!</p>
<p>Daqui a pouquinho chegamos aí, estamos ansioso pra te conhecer!</p>
<p>Segue o link do boleto:<br />${props.link}</p>
<p>No Internet Banking, utilize o código de barras:<br /><code>${props.code}</code></p>
<p>Caso já tenha efetuado o pagamento ao ler este e-mail, por favor, desconsidere essa mensagem.</p>
<p>Qualquer dúvida é só nos chamar,<br />Beijos e até breve</p>
`, '#5668e6', '#FFFFFF')

  return {
    subject: 'Hoje vence seu Boleto - Curitiba na Caixa',
    body: content,
  }
}
