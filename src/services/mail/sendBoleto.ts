import NewOrderBoleto from '@/services/mail/content/NewOrderBoleto'
import sendmail from '@/services/mail/sendmail'

/* eslint-disable no-console */
export default async (transaction) => {
  if (transaction.payment_method !== 'boleto') return
  if (!transaction.boleto_url) return
  if (!transaction.customer.email) return

  const { email, name } = transaction.customer
  const { items, amount } = transaction
  const content = NewOrderBoleto({
    name,
    number: transaction.id,
    link: transaction.boleto_url,
    code: transaction.boleto_barcode,
    items,
    amount,
  })
  await sendmail(email, content)
}
