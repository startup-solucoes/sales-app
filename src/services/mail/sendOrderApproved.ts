import OrderApproved from '@/services/mail/content/OrderApproved'
import sendmail from '@/services/mail/sendmail'

/* eslint-disable no-console */
export default async (transaction) => {
  if (transaction.subscription_id) return
  // if (transaction.payment_method !== 'credit_card') return
  if (transaction.status !== 'paid') return
  if (!transaction.customer.email) return

  const { email, name } = transaction.customer
  const { items, amount, id } = transaction
  const content = OrderApproved({
    name,
    items,
    amount,
    number: id,
  })
  await sendmail(email, content)
}
