import sendmail from '@/services/mail/sendmail'
import Unsubscribe from '@/services/mail/content/Unsubscribe'

/* eslint-disable no-console */
export default async (transaction) => {
  if (transaction.object !== 'subscription') return
  if (!transaction.customer.email) return

  const { email, name } = transaction.customer
  const content = Unsubscribe({
    name,
    link: transaction.manage_url,
  })
  await sendmail(email, content)
}
