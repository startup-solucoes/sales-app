import sendBoleto from '@/services/mail/sendBoleto'
import sendBoletoReminder from '@/services/mail/sendBoletoReminder'
import sendBoletoReminderLastDay from '@/services/mail/sendBoletoReminderLastDay'
import sendOrderApproved from '@/services/mail/sendOrderApproved'
import sendNewSubscription from '@/services/mail/sendNewSubscription'
import sendUnsubscribe from '@/services/mail/sendUnsubscribe'

export {
  sendOrderApproved,
  sendNewSubscription,
  sendUnsubscribe,
  sendBoleto,
  sendBoletoReminder,
  sendBoletoReminderLastDay,
}
