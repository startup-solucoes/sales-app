export default (title: string, body: string, bgColor: string = '#CCCCCC', textColor: string = '#000000') : string => `
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Curitiba na Caixa</title>

    <style type="text/css">
      html, body, .body {
        background-color: #EEEEEE;
      }
      table {
        border-collapse: collapse;
        mso-table-lspace: 0pt;
        mso-table-rspace: 0pt;
        background-color: #FFFFFF;
      } /* Remove o espaçamento entorno das tabelas no Outlook 07, 10 */
      table td {
        border-collapse: collapse;
        background-color: #FFFFFF;
      } /* Corrige o padding no Outlook 07, 10 */
      .logo {
        padding: 32px;
        text-align: center;
      }
      .header__title {
        padding: 50px 32px;
        background-color: ${bgColor};
        text-align: center;
        text-transform: uppercase;
        font-size: 30px;
        font-weight: bold;
        color: ${textColor};
      }
      .content {
        padding: 40px 32px;
        font-size: 16px;
        line-height: 1.5;
        color: #333333;
      }
      .order-details {
        margin: 32px 0;
      }
      .order-details thead td {
        padding: 6px;
        border-bottom: 2px solid #333333;
        font-weight: bold;
      }
      .order-details tbody td {
        padding: 6px;
        border-bottom: 1px solid #AAAAAA;
      }
      .footer {
        padding: 50px 16px;
        text-align: center;
        font-size: 16px;
        color: #000000;
      }
      .footer a {
        text-decoration: none;
        color: #000000;
      }
      .align-right {
        text-align: right;
      }
      @media only screen and (max-width: 600px) {
        table[class="content-wrap"] {
          width: 90%;
        }
      }
    </style>
  </head>
  <body
    bgcolor="#EEEEEE"
    leftmargin="0"
    topmargin="0"
    marginwidth="0"
    marginheight="0"
    class="body"
  >
    <table
      width="600"
      cellpadding="0"
      cellspacing="0"
      border="0"
      align="center"
      class="content-wrap"
    >
      <tr>
        <td class="logo">
          <center><img src="https://curitibanacaixa.com.br/images/favicon/curitiba-na-caixa-180x180.png" width="150" height="150" /></center>
        </td>
      </tr>
      <tr>
        <td class="header__title">
          <center><span><strong>${title}</strong></span></center>
        </td>
      </tr>
      <tr>
        <td class="content">
          ${body}
        </td>
      </tr>
      <tr>
        <td class="footer">
          <center>www.curitibanacaixa.com.br</center>
        </td>
      </tr>
    </table>
  </body>
</html>
`
