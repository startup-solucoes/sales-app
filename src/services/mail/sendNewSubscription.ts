import NewSubscription from '@/services/mail/content/NewSubscription'
import sendmail from '@/services/mail/sendmail'

/* eslint-disable no-console */
export default async (transaction) => {
  if (transaction.object !== 'subscription') return
  if (transaction.status !== 'paid') return
  if (!transaction.customer.email) return

  const { email, name } = transaction.customer
  const { plan } = transaction
  const content = NewSubscription({
    name,
    number: transaction.id,
    plan,
  })
  await sendmail(email, content)
}
