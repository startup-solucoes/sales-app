/* eslint-disable lines-between-class-members */

export interface ApiResponse {
  success: boolean;
  statusCode: number;
  data?: any;
  message?: string;
  errors?: any[];
}
