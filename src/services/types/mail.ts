export type MailContent = {
  body: string,
  subject: string,
}
