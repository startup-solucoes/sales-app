import Document, {
  Html, Head, Main, NextScript, DocumentContext,
} from 'next/document'

class MyDocument extends Document {
  static async getInitialProps(ctx: DocumentContext) {
    const initialProps = await Document.getInitialProps(ctx)
    return initialProps
  }

  render() {
    return (
      <Html lang="pt-BR">
        <Head>
          <meta charSet="utf-8" />
          <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
          <meta name="description" content="Uma caixa recheada de novos sabores e experiências únicas sem precisar sair de casa." />
          <meta name="theme-color" content="#CA0253" />
          <link rel="icon" href="/_next/image?url=%2Fimages%2Ffavicon%2Fcuritiba-na-caixa-32x32.png&w=32&q=75" sizes="32x32" />
          <link rel="icon" href="/_next/image?url=%2Fimages%2Ffavicon%2Fcuritiba-na-caixa-192x192.png&w=192&q=75" sizes="192x192" />
          <link rel="apple-touch-icon" href="/_next/image?url=%2Fimages%2Ffavicon%2Fcuritiba-na-caixa-192x192.png&w=192&q=75" />
          <meta name="msapplication-TileImage" content="/_next/image?url=%2Fimages%2Ffavicon%2Fcuritiba-na-caixa-180x180.png&w=180&q=75" />
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}

export default MyDocument
