import type { NextApiRequest, NextApiResponse } from 'next'
import abandonedCart from '@/services/payment/abandoned-cart'

export default async (req: NextApiRequest, res: NextApiResponse) => {
  try {
    const transaction = req.body
    const message = await abandonedCart(transaction)
    res.status(200).json(message)
  } catch (error) {
    res.status(500).json({ error })
  }
}
