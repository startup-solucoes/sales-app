/* eslint-disable no-console */
import type { NextApiRequest, NextApiResponse } from 'next'
import subscription from '@/services/payment/subscription'
import { sendNewSubscription } from '@/services/mail'

export default async (req: NextApiRequest, res: NextApiResponse) => {
  try {
    const checkout = req.body
    checkout.plan_id = process.env.PAGARME_PLAN_ID
    // (process.env.NODE_ENV === 'development')
    //   ? 551335
    //   : 1213281
    const transaction = await subscription(checkout)
    console.log(transaction)

    /**
     * Envia o Email de Nova Assinatura
     */
    await sendNewSubscription(transaction)

    return res.status(200).json(transaction)
  } catch (error) {
    return res.status(500).json({ error })
  }
}
