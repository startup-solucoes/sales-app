/* eslint-disable no-console */
import type { NextApiRequest, NextApiResponse } from 'next'
import postback from '@/services/payment/postback'
import getTransaction from '@/services/payment/transaction'
import { sendOrderApproved } from '@/services/mail'

export default async (req: NextApiRequest, res: NextApiResponse) => {
  try {
    /**
     * Valida o Postback e verifica o status
     */
    const result = await postback(req)

    const transactionId = req.body.id
    const transaction = await getTransaction(transactionId)
    console.log(transaction)

    /**
     * Envia o Email de Nova Assinatura
     */
    await sendOrderApproved(transaction)

    /**
     * Retorna a resposta em JSON
     */
    res.status(result.statusCode).json(result.body)
  } catch (error) {
    console.log(error)
    res.status(error.statusCode).json(error.body)
  }
}
