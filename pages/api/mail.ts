/* eslint-disable no-console */
import type { NextApiRequest, NextApiResponse } from 'next'
import sendmail from '@/services/mail/sendmail'
import OrderApproved from '@/services/mail/content/OrderApproved'

export default async (req: NextApiRequest, res: NextApiResponse) => {
  try {
    const { email } = req.body

    const items = [
      {
        category: null,
        date: null,
        id: '1',
        object: 'item',
        quantity: 1,
        tangible: true,
        title: 'Plano único',
        unit_price: 9900,
        venue: null,
      },
    ]
    const content = OrderApproved({
      name: 'Giovanni',
      number: 123456,
      amount: 11400,
      items,
    })
    console.log(content)
    const send = await sendmail(email, content)

    console.log(send)
    res.status(200).json({ ...send, content })
  } catch (error) {
    res.status(500).json({ error })
  }
}
