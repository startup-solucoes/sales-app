/* eslint-disable no-console */
import type { NextApiRequest, NextApiResponse } from 'next'

import capture from '@/services/payment/capture'
import { sendBoleto } from '@/services/mail'

export default async (req: NextApiRequest, res: NextApiResponse) => {
  try {
    const transaction = await capture(req.body)

    /**
     * Envia o Email com o Boleto
     * Somente para payment_method igual a boleto
     */
    await sendBoleto(transaction)

    /**
     * Envia o Email de Novo Pedido
     * Somente para payment_method igual a credit_card
     * E o status for igual a paid
     */
    // await sendNewOrder(transaction)

    res.status(200).json(transaction)
  } catch (error) {
    res.status(500).json({ error })
  }
}
