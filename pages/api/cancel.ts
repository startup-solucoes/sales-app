/* eslint-disable no-console */
import type { NextApiRequest, NextApiResponse } from 'next'
import { searchSubscription } from '@/services/payment/unsubscription'
import { sendUnsubscribe } from '@/services/mail'

export default async (req: NextApiRequest, res: NextApiResponse) => {
  try {
    const { email } = req.body
    const subscription = await searchSubscription(email)
    if (subscription) {
      /**
       * Envia o Email
       */
      await sendUnsubscribe(subscription)

      const { id, customer } = subscription
      res.status(200).json({
        id,
        name: customer.name,
      })
    } else {
      res.status(404).json({
        success: false,
        statusCode: 404,
        message: 'Nenhuma assinatura encontrada',
      })
    }
  } catch (error) {
    res.status(500).json({ error })
  }
}
