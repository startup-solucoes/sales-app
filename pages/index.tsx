/* eslint-disable react/jsx-props-no-spreading */
import { GetStaticProps } from 'next'
import { Countdown } from '@/helpers/countdown'
import HomeScreen from '@/screens/Home'

export default function Home(props) {
  return <HomeScreen {...props} />
}

export const getStaticProps: GetStaticProps = async () => {
  const sendDateText = '31 de agosto'
  const finalDateString = '2021-08-28T10:00:00-03:00'
  const isClosedSales = false
  const cronometro = Countdown(finalDateString)

  return {
    revalidate: 60,
    props: {
      cronometro,
      sendDateText,
      isClosedSales,
    },
  };
}
